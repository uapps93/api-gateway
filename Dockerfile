FROM node:12-alpine

# create app directory
WORKDIR /app

COPY package.json ./

RUN npm install

# Bundle app source
COPY . .

# Add Environment Variable
ENV NODE_ENV=development

# Run the command on container startup
ENTRYPOINT ["npm", "start"]

