const express = require('express')
const router  = express.Router()
const api     = require('../apis')

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  // check JWT
  // check reference token

  // replace id
  // do not replace username
  next()
})

// define auth login
router.all('/user/update', api.route)
router.all('/user/update/*', api.route)

module.exports = router