const express = require('express')
const router  = express.Router()
const api     = require('../apis')
const { getToken } = require('../libs/utils')

// define all routing
router.all('/api/oauth/token', api.route)
router.all('/api/oauth/refresh', api.route)
router.all('/api/oauth/refreshtoken', api.route)
router.all('/api/login', api.route)
router.all('/api/register', api.route)
router.all('/api/register/check', api.route)

module.exports = router