module.exports = {
	auth: require ('./auth'),
	update: require ('./auth_update'),
    public: require('./public'),
	api: require('./apis'),
}