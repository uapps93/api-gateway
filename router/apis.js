const express = require('express')
const router  = express.Router()
const api     = require('../apis')
const { getToken } = require('../libs/utils')

// middleware that is specific to this router
// router.use(function timeLog (req, res, next) {
//   // Checking JWT
//   // Checking reference Token

//   // replace id
//   // replace username
//   next()
// })


async function isAuth(req, res, next) {
	console.log('WHOA WHO ARE YOU?')

	let token = getToken(req)
	if ( null === token) {
		return res.status(401).json({
			status: false,
			data: {
				message: 'AUTHENTICATION REQUIRED'
			}
		})
	}

	let authenticated = await api.checkAuth(token, 'reference-token')

	if ( ! authenticated.status) {
		return res.status(401).json({
			status: false,
			data: {
				message: 'NOT AUTHENTICATED'
			}
		})
	}

	next()
}

// define all routing
router.all('/*', isAuth, api.route)

module.exports = router