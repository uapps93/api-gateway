const express = require('express')
const router  = express.Router()
const api     = require('../apis')

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

// define auth login
router.all('/user/login', api.route)
router.all('/user/login/*', api.route)

router.all('/user/signup', api.route)
router.all('/user/signup/*', api.route)

module.exports = router