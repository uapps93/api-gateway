const needle = require('needle')
const crypto = require('crypto')

class Main {
	async request(method, url, data, options) {
	  try {
	    console.log(`[HTTP][REQ][OUT] ${url}`)
	    console.log(`[HTTP][REQ][OUT] ${JSON.stringify(data)}`)
	    console.log(`[HTTP][REQ][OUT] ${JSON.stringify(options)}`)

	    let res = await needle(method, url, data, options)

	    console.log(`[HTTP][RES][IN] ${res.statusCode}`)
	    console.log(res.body)

	    if (undefined === res.body) {
	      return res.statusCode
	    }

	    console.log(`[HTTP][RES][IN] ${JSON.stringify(res.body)}`)

	    return res.body

	  } catch (e) {
	    console.error(e.stack)
	    return null
	  }
	}

	getToken(req) {
	  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
	    return req.headers.authorization.split(' ')[1];
	  }

	  return null
	}

	errorReff() {
	    return crypto.randomBytes(3).toString('hex');
	}
	
}

module.exports = new Main()