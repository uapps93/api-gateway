const express       = require('express')
const fs            = require('fs');
const uuid          = require('uuid').v4
const args          = require('minimist')(process.argv.slice(2));
const iniParser     = require('./libs/iniParser');
const logging       = require('./libs/logging');
const api 			= require('./apis');
const router        = require('./router')
const app           = express()

const NODE_ENV		= process.env.NODE_ENV

const defaultConfigFile = ('production' === NODE_ENV) ? './configs/config.ini' : './configs/dev.ini'

let config = {
    log: {
        path: 'var/log/',
        level: 'debug',
        type: 'file',
        errorSufix: '-error',
        filename: 'gateway'
    },
    app: {
    	host: '127.0.0.1',
    	port: 80
    }
}

/* Initialise Config */
let configFile = args.c || args.config || defaultConfigFile;
config = iniParser.init(config, configFile);
config.log.level = args.logLevel || config.log.level;

/** Initalise logging*/
logging.init(config.log)

/* Initialise Apis */
api.init()

// app.use(express.urlencoded({ extended: true }));
// app.use(express.json());

app.use(function (req, res, next) {
    const { rawHeaders, httpVersion, method, socket, url } = req;
    const { remoteAddress, remoteFamily } = socket || {};
    let dataReq = JSON.stringify({
        timestamp: Date.now(),
        rawHeaders,
        httpVersion,
        method,
        remoteAddress,
        remoteFamily,
        url
      })

    let uid = uuid()
    // if (undefined === req.body.uuid) req.body.uuid = uuid()

    logging.http(`[IN][REQUEST] [${uid}] ${dataReq}`);
    logging.http(`[IN][REQUEST][BODY] [${uid}] ${JSON.stringify(req.body)}`);
    next();
});

/* Routes the request */
app.use(router.public)
app.use(router.auth)
app.use(router.api)

app.listen(config.app.port, () => {
	logging.info(`[APP] Gateway started on PORT : ${config.app.port} ENV : ${NODE_ENV}`)
});

process
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
  })
  .on('uncaughtException', err => {
    console.error(err, 'Uncaught Exception thrown');
  });
