const httpProxy = require('http-proxy')
const { Readable } = require('stream')

const proxy     = httpProxy.createProxyServer()
const iniParser = require('../libs/iniParser');
const logging   = require('../libs/logging');

let config

function init() {
	config = iniParser.get()
}

function route(req, res) {
	logging.http(`[route] to roter ${config.router.url}`)


	let options = {
		target: config.router.url,
	}

	logging.debug(`[PROXY] [OPTIONS] ${JSON.stringify(options)}`)

	proxy.web(req, res, options)
}

function addBody(body) {
	body.id 	  = 666
	body.username = 'dev'

	logging.debug(`[PROXY][NEW BODY] ${JSON.stringify(body)}`)

	return Buffer.from(JSON.stringify(body))
}

function toStream(buffer) {
	let readable = new Readable()
	readable._read = () => {} // _read is required but you can noop it
	readable.push(buffer)
	readable.push(null)

	return readable
}


module.exports = {
	init,
	route
}