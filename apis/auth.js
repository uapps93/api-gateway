const iniParser     = require('../libs/iniParser');
const logging       = require('../libs/logging');
const proxy         = require('./proxy')
let config

function init() {
	config = iniParser.get()
}

function login(req, res) {
	proxy.route(req, res)
}

module.exports = {
	init,
	login
}