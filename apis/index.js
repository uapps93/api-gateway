const iniParser     = require('../libs/iniParser');
const logging       = require('../libs/logging');
const auth         	= require('./auth')
const all       	= require('./all')
const proxy       	= require('./proxy')
const { request }    = require('../libs/utils')

let config

function init() {
	config = iniParser.get()
	auth.init()
	proxy.init()
}

function bypass (req, res) {
	return proxy.route(req, res)
}

function route (req, res) {
	logging.debug('ROUTE FROM ' + req.path)
	return proxy.route(req, res)
}

async function checkAuth(jwt, reference) {
	let url = `${config.router.url}/user/check`
	let data = {
		jwt,
		reference
	}

	return await request('POST', url, data)
}

module.exports = {
	init,
	bypass,
	route,
	checkAuth
}